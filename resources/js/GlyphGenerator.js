class CharacterGenerator{
    constructor(){
        this.language_complexity = 0;
        this.language_style = '';
        this.language_orientation = '';
    }

    //Generates rules for the language, not the characters themselves
    GenerateLanguageProperties(){
        if(this.language_complexity == 0){
            this.language_complexity = Math.floor((Math.random() * 3) + 1);
        }
        if(this.language_style == ''){
            var number = Math.floor(Math.random() * 3);
            if(number == 0){
                this.language_style = 'rigid';
            }
            else if(number == 1){
                this.language_style = 'smooth';
            }
            else if(number == 2){
                this.language_style = 'mixed';
            }
        }
        if(this.language_orientation == ''){
            var number = Math.floor(Math.random() * 4);
            if(number == 0){
                this.language_orientation = 'right_to_left';
            }
            else if(number == 1){
                this.language_orientation = 'left_to_right';
            }
            else if(number == 2){
                this.language_orientation = 'top_to_bottom';
            }
            else if(number == 3){
                this.language_orientation = 'bottom_to_top';
            }
        }
    }
    GenerateCharacter(){
        switch(this.language_style){
            case 'rigid':
                this.GenerateRigidCharacter();
                break;
            case 'smooth':
                this.GenerateSmoothCharacter();
                break;
            default:
                this.GenerateMixedCharacter();
                break;
        }
    }
    GenerateRigidCharacter(){
        var total_strokes = this.GetComplexity();
        var stroke = 0;
        var contiguous = 0;
        var shape = -1;
        while(stroke < total_strokes){
            //contiguous will determine if the stoke will be disjoint of contiguous, for rigid characters there is a 2/3 chance for the line to be disjoint and a 1/3 chance of being contiguous
            contiguous = Math.floor(Math.random() * 3);
            //disjoint
            if(contiguous < 2){
                shape = Math.floor(Math.random() * 6);
                if(shape < 4){
                    DrawLine();
                }
                else if(shape < 5){
                    DrawSquare();
                }
                else{
                    DrawTriangle();
                }
            }
            //contiguous
            else{

            }
            stroke++;
        }
    }
    GenerateSmoothCharacter(){
        var total_strokes = this.GetComplexity();
        var stroke = 0;
        var contiguous = 0;
        while(stroke < total_strokes){
            //contiguous will determine if the stoke will be disjoint of contiguous, for smooth characters there is a 3/4 chance for the line to be contiguous and a 1/4 chance of being disjoint
            contiguous = Math.floor(Math.random() * 4);
            //contiguous
            if(contiguous < 3){

            }
            //disjoint
            else{

            }
            stroke++;
        }
    }
    GenerateMixedCharacter(){
        var total_strokes = this.GetComplexity();
        var stroke = 0;
        var contiguous = 0;
        while(stroke < total_strokes){
            //contiguous will determine if the stoke will be disjoint of contiguous, for mixed characters there is a 2/3 chance for the line to be contiguous and a 1/3 chance of being disjoint
            contiguous = Math.floor(Math.random() * 3);
            //contiguous
            if(contiguous < 2){

            }
            //disjoint
            else{

            }
            stroke++;
        }
    }
    GetComplexity(){
        return Math.floor(Math.random() * 4) + this.language_complexity;
    }
}
